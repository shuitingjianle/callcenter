//
//  HelloWorldViewController.m
//  HelloWorld
//
//  Created by tester on 11/20/13.
//  Copyright (c) 2013 tester. All rights reserved.
//

#import "HelloWorldViewController.h"

@interface HelloWorldViewController ()

@end

@implementation HelloWorldViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)showMessage
{
    UIAlertView *helloWorldAlert = [[UIAlertView alloc]initWithTitle:@"My First App" message:@"Hello, World" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    int a = 1;
    int b = a;
    
    [helloWorldAlert show];
}

@end
