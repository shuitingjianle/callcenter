//
//  HelloWorldViewController.h
//  HelloWorld
//
//  Created by tester on 11/20/13.
//  Copyright (c) 2013 tester. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelloWorldViewController : UIViewController
-(IBAction)showMessage;
@end
